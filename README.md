This is a vim syntax plugin for the [Nomsu programming language](https://nomsu.org).
I recommend using [vim-plug](https://github.com/junegunn/vim-plug) to install it:

```
Plug 'https://bitbucket.org/spilt/vim-nomsu', { 'for': 'nomsu' }
```
