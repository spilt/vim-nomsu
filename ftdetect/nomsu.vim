" Language:    Nomsu
" Maintainer:  Bruce Hill <bruce@bruce-hill.com>
" License:     WTFPL

autocmd BufNewFile,BufRead *.nom set filetype=nomsu
