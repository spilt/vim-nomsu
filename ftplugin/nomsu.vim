" Language:    Nomsu
" Maintainer:  Bruce Hill <bruce@bruce-hill.com>
" License:     WTFPL

if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1

setlocal formatoptions-=t
setlocal iskeyword+=$
setlocal cpoptions+=M
setlocal foldmethod=indent
setlocal formatprg=nomsu\ -t\ format\ -q\ -
setlocal expandtab
setlocal tabstop=4
setlocal shiftwidth=0
setlocal commentstring=//\ %s
normal zR

command! NomsuHideTests :g/^\s*test:$/exe "normal! jzc"

let b:undo_ftplugin = "setlocal formatoptions< iskeyword< cpoptions< foldmethod<"
