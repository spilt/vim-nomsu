" Language:    Nomsu
" Maintainer:  Bruce Hill <bruce@bruce-hill.com>
" License:     WTFPL

if exists("b:did_indent")
  finish
endif

let b:did_indent = 1

setlocal autoindent
setlocal indentexpr=GetNomsuIndent()
setlocal indentkeys+=0],0),1),1.

" Only define the function once.
if exists("*GetNomsuIndent")
  finish
endif

function! GetNomsuIndent()
  let line = getline(v:lnum)
  let current_ind = indent(v:lnum)
  let previousNum = prevnonblank(v:lnum - 1)
  let previous = getline(previousNum)
  let ind = indent(previousNum)

  " TODO: don't auto-indent inside a multi-line comment?
  if previous =~ '\%([{[(:]\|("\)$' && previous !~ '^\s*#'
    let ind += &tabstop
  endif

  " TODO: dedent to matching parens, don't just dedent once
  if line =~ '^\s*\%([\])}]\|")\|\.\.\)'
    "let ind -= &tabstop
    return current_ind - &tabstop
  endif

  if ind == indent(previousNum)
      return current_ind
  endif
  return ind
endfunction
