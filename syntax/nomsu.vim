" Language:    Nomsu
" Maintainer:  Bruce Hill <bruce@bruce-hill.com>
" License:     WTFPL

" Bail if our syntax is already loaded.
if exists('b:current_syntax') && b:current_syntax == 'nomsu'
  finish
endif

" Highlight long strings.
syn sync minlines=100

"hi def link nomsuInterpDelim PreProc
hi nomsuInterpDelim ctermfg=Gray
syn region nomsuLineComment start=;//; end=/$/
hi nomsuLineComment ctermfg=DarkBlue
hi def link nomsuLineComment Comment

syn region nomsuList start=/\[/ end=/\]/ contains=@nomsuAll
hi nomsuList ctermfg=Blue

syn region nomsuDict start=/{/ end=/}/ contains=@nomsuAll
hi nomsuDict ctermfg=Green

syn region nomsuParenGroup start=/(/ end=/)/ contains=@nomsuAll
hi nomsuParenGroup ctermfg=Yellow

syn match nomsuTextBackslash /\\\%(\\\|;\)\@=/ nextgroup=nomsuTextEscapedBackslash contained
hi nomsuTextBackslash ctermfg=Gray
syn match nomsuTextEscapedChar /\%(\\\)\@<=\%(\x\{1,3}\|x\x\{1,2}\|.\)/ contained
hi nomsuTextEscapedChar cterm=bold ctermfg=Blue
syn match nomsuTextEscapedBackslash /./ contained
hi nomsuTextEscapedBackslash cterm=bold ctermfg=Blue
syn match nomsuTextEscape /\\\%(\x\{1,3}\|x\x\{1,2}\|.\)/ contains=nomsuTextEscapedChar contained
hi nomsuTextEscape ctermfg=Gray
syn match nomsuInterp /\\\%([[{($:]\)\@=/ 
\           nextgroup=nomsuActionVar,nomsuVar,nomsuList,nomsuDict,nomsuParenGroup,nomsuIndentedInterpBlock contained
hi nomsuInterp ctermfg=Gray
syn region nomsuInterpDotDot matchgroup=nomsuInterpDelim start=/\\\s*$/ end=/\.\./ contained
hi nomsuInterpDotDot ctermfg=Gray
syn region nomsuIndentedText matchgroup=nomsuTextQuotes start=/\%(^\z(\(\s*\)\)[^\n]*\)\@<=("$/ end=/^\z1")/
\           contains=nomsuInterp,nomsuInterpDotDot,nomsuTextBackslash
hi def link nomsuIndentedText String
hi nomsuTextQuotes ctermfg=DarkMagenta

syn region nomsuText matchgroup=nomsuTextQuotes start=/\%((\)\@<!"\|"\%($\)\@!/ end=/"\|$/ contains=nomsuInterp,nomsuTextEscape
hi def link nomsuText String

syn region nomsuIndentedBlock start=/\%(^\z(\s*\)[^\n]*\)\@<=:\s*$/ end=/\n\%(\z1\s\)\@!\%(\s*\.\.\)\?/
\           contains=@nomsuAll
syn region nomsuIndentedInterpBlock matchgroup=nomsuColon start=/:\s*$/ end=/\n\s*\.\./ contained
\           contains=@nomsuAll

syn match nomsuVar /\$\%(\%([a-zA-Z0-9_-]\|[^\x00-\xff]\)\+\|(\@!\)/
\          contains=nomsuDollarSign,nomsuVarParens
hi nomsuVar ctermfg=Cyan
hi def link nomsuVar Identifier

syn region nomsuActionVar matchgroup=nomsuVar start=/\$(/ end=/)\|$/ contains=nomsuVarWord,nomsuVarOpWord,@nomsuAll

syn match nomsuDollarSign /\$/ contained
hi nomsuDollarSign ctermfg=Cyan

syn match nomsuVarParens /[()]/ contained
hi nomsuVarParens ctermfg=LightCyan

syn match nomsuWord /\%([a-zA-Z_]\|[^\x00-\xff]\)\%([a-zA-Z0-9_]\|[^\x00-\xff]\)*/
hi def link nomsuWord Function
hi nomsuWord ctermfg=White
syn match nomsuOpWord ;\(\(//\)\@![`~!|%#@^&*_+=<>?'/-]\)\+\|\%(^\s*\)\@<!#;
hi def link nomsuOpWord Function
hi nomsuOpWord ctermfg=LightYellow

syn match nomsuVarWord /\%([a-zA-Z_]\|[^\x00-\xff]\)\%([a-zA-Z0-9_]\|[^\x00-\xff]\)*/ contained
hi def link nomsuVarWord Function
hi nomsuVarWord ctermfg=LightCyan
syn match nomsuVarOpWord ;\(\(//\)\@![`~!|%#@^&*_+=<>?'/-]\)\+\|\%(^\s*\)\@<!#; contained
hi def link nomsuVarOpWord Function
hi nomsuVarOpWord ctermfg=LightCyan

syn match nomsuNumber /-\?\%(0x\x\+\|\d\+\%(\.\%(\d\+\)\?\)\?\|\.\d\+\)/
hi def link nomsuNumber Number
hi nomsuNumber ctermfg=Red

syn match nomsuInt /-\?\%(0x\x\+\|\d\+\)/
hi def link nomsuInt Number
hi nomsuInt ctermfg=Red

syn match nomsuTextWord /\%([a-zA-Z_]\|[^\x00-\xff]\)\%([a-zA-Z0-9_]\|[^\x00-\xff]\)*/ contained
hi def link nomsuTextWord String

syn match nomsuIndex /\./ nextgroup=nomsuTextWord,nomsuParenGroup,nomsuText,nomsuIndentedText,
            \nomsuVar,nomsuActionVar,nomsuInt,nomsuDict,nomsuList,nomsuIndentedBlock
hi nomsuIndex ctermfg=Yellow

syn match nomsuDotDot /\.\./
hi def link nomsuDotDot Special
hi nomsuDotDot ctermfg=DarkGray

syn match nomsuComma /,/
hi nomsuComma ctermfg=Blue
hi def link nomsuComma Delimiter

syn match nomsuColon /:/
hi nomsuColon ctermfg=Yellow
hi def link nomsuColon Delimiter

syn match nomsuSemicolon /;/
hi nomsuSemicolon ctermfg=Blue
hi def link nomsuSemicolon Delimiter

syn region nomsuComment start=/\%(^\z(\s*\)\)\@<=#/ skip=/\n\s*$/ end=/\n\%(\z1\s\)\@!/
hi nomsuComment ctermfg=DarkBlue
hi def link nomsuComment Comment

syn cluster nomsuExpr contains=nomsuParenGroup,nomsuText,nomsuIndentedText,nomsuVar,
            \nomsuActionVar,nomsuNumber,nomsuDict,nomsuList,nomsuIndentedBlock

syn cluster nomsuAll contains=nomsuParenGroup,nomsuText,nomsuIndentedText,nomsuComment,nomsuLineComment,
\             nomsuVar,nomsuActionVar,nomsuNumber,nomsuDict,nomsuList,nomsuIndentedBlock,
\             nomsuColon,nomsuSemicolon,nomsuDotDot,nomsuWord,nomsuOpWord,nomsuComma,nomsuIndex

if !exists('b:current_syntax')
  let b:current_syntax = 'nomsu'
endif
